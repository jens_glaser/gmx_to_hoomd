import sys
import math

import numpy

class pdb:
    def __init__(self):
        self.pos = []
        self.chain_atoms = []
        self.chain_ids = []
        self.biomt = []
        self.b_factors = []
        self.residues = []
        self.res_seq = []

        self.lx = 1
        self.ly = 1
        self.lz = 1
        self.xy = 0
        self.xz = 0
        self.yz = 0
        self.lattice_vectors = [(1,0,0),(0,1,0),(0,0,1)]

    def read(self,filename):
        cur_chain_atoms = []
        cur_chain_id = ' '
        cur_atom_offset = 0
        with open(filename,'r') as f:

            while True:
                l = f.readline()
                if l == "":
                    break
                tokens=l.split()
                if tokens[0] == "REMARK":
                    if len(tokens) > 1 and tokens[1] == "350":
                        if len(tokens) > 2 and tokens[2][:-1] == "BIOMT":
                            biomt_num = int(tokens[2][-1])
                            biomt_line = (tokens[4],tokens[5],tokens[6],tokens[7])
                            biomt_line = [float(fl) for fl in biomt_line]
                            if biomt_num == 1:
                                self.biomt.append([biomt_line])
                            else:
                                self.biomt[-1].append(biomt_line)

                            if biomt_num != len(self.biomt[-1]) or int(tokens[3]) != len(self.biomt):
                                raise RuntimeError("Wrong order of BIOMT entries")

                if tokens[0] == "ATOM":
                    if len(l) < 66:
                        raise RuntimeError("Wrong length of ATOM entry (%d)" % len(l))
                    (x,y,z) = (float(l[30:38]),float(l[38:46]),float(l[46:54]))
                    b = float(l[61:66])
                    res = l[17:20]
                    res_seq = int(l[22:26])

                    self.pos.append((x,y,z))
                    self.b_factors.append(b)
                    self.residues.append(res)
                    self.res_seq.append(res_seq)

                    chain_id = l[21]
#                    if chain_id == ' ':
#                        # use residue name
#                        chain_id = l[17:20]

                    if cur_chain_id != chain_id:
                        if cur_chain_id != ' ':
                            if cur_chain_id not in self.chain_ids:
                                self.chain_atoms.append(cur_chain_atoms)
                                self.chain_ids.append(cur_chain_id)
                            else:
                                i = 0
                                for cid in self.chain_ids:
                                    if cid == cur_chain_id:
                                        break
                                    i += 1
                                self.chain_atoms[i].extend(cur_chain_atoms)

                        cur_chain_atoms = []
                        cur_chain_id = chain_id

                    atom_id = int(l[6:11])
                    cur_chain_atoms.append(atom_id-1+cur_atom_offset)
                    if atom_id == 99999:
                        cur_atom_offset += 100000

                if tokens[0] == "TER":
                    self.chain_atoms.append(cur_chain_atoms)
                    self.chain_ids.append(cur_chain_id)
                    cur_chain_atoms = []
                    cur_chain_id = ' '

                if tokens[0] == "CRYST1":
                    if len(l) < 70:
                        raise RuntimeError("Wrong length of CRYST1 entry (%d)" % len(l))

                    cryst = [float(l[6:15]), float(l[15:24]), float(l[24:33]),
                        float(l[33:40]),float(l[40:47]), float(l[47:54]),
                        str(l[55:66]), int(l[66:70])]

                    # construct a box matrix
                    (a,b,c) = (cryst[0], cryst[1], cryst[2])
                    (alpha, beta, gamma) = (math.pi/180.0*angle for angle in (cryst[3], cryst[4], cryst[5]))
                    # radians

                    self.lx = a
                    self.xy = b*math.cos(gamma)
                    self.xz = c*math.cos(beta)
                    self.ly = math.sqrt(b*b-self.xy*self.xy)
                    self.yz = (b*c*math.cos(alpha)-self.xy*self.xz)/self.ly
                    self.lz = math.sqrt(c*c-self.xz*self.xz-self.yz*self.yz)

                    self.lattice_vectors=numpy.array([[self.lx,0,0],[self.xy,self.ly,0],[self.xz,self.yz,self.lz]])

                    self.xy /= self.ly
                    self.xz /= self.lz
                    self.yz /= self.lz

